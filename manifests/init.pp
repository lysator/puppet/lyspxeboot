class lyspxeboot {
  $pxe_ip = '172.16.16.1'
  $pxe_network = '172.16.16.0'
  $pxe_netmask = '255.255.255.0'
  $pxe_cidr_prefix = '/24'
  $pxe_dhcp_range_start = '172.16.16.10'
  $pxe_dhcp_range_end = '172.16.16.250'

  # Configure the pxe interface
  ::lysnetwork::systemd_networkd::interface { 'eth1':
    addresses => ["${pxe_ip}${pxe_cidr_prefix}"],
  }

  $debian_codename = 'bookworm'

  # NOTE: fqdn must be present in DNS
  $automatic_hosts = [
    { mac => '20:cf:30:c3:3e:8f', fqdn => 'rakanishu.lysator.liu.se', },
    { mac => '20:cf:30:c3:3e:7d', fqdn => 'bishibosh.lysator.liu.se', },
    { mac => 'a4:ba:db:02:69:34', fqdn => 'graf.lysator.liu.se', },
    { mac => 'ec:b1:d7:64:1f:ce', fqdn => 'hakumei.lysator.liu.se', },
    { mac => '78:24:af:8c:e1:43', fqdn => 'borsalino.lysator.liu.se', },
    { mac => 'ec:b1:d7:64:1f:e8', fqdn => 'mikochi.lysator.liu.se', },
    { mac => 'bc:ee:7b:e0:cd:cd', fqdn => 'artemis.lysator.liu.se', },
    { mac => 'bc:ee:7b:e0:ce:64', fqdn => 'eris.lysator.liu.se', },
    { mac => 'bc:ee:7b:e0:cf:2c', fqdn => 'hades.lysator.liu.se', },
    { mac => '00:50:8b:ac:09:c4', fqdn => 'lain.lysator.liu.se', },
    { mac => '20:cf:30:04:9a:3d', fqdn => 'lain.lysator.liu.se', },
    { mac => '40:2c:f4:e9:c8:59', fqdn => 'claptrap.lysator.liu.se', },
    { mac => '00:23:7d:dc:b6:b4', fqdn => 'slartibartfast.lysator.liu.se', },
    { mac => '84:2b:2b:74:84:1f', fqdn => 'trillian-0.lysator.liu.se', },
    { mac => '84:2b:2b:73:18:eb', fqdn => 'trillian-1.lysator.liu.se', },
    { mac => '84:2b:2b:72:43:82', fqdn => 'trillian-2.lysator.liu.se', },
    { mac => '84:2b:2b:74:81:29', fqdn => 'trillian-3.lysator.liu.se', },
    { mac => '84:2b:2b:73:1a:28', fqdn => 'trillian-4.lysator.liu.se', },
    { mac => '84:2b:2b:73:38:4d', fqdn => 'trillian-5.lysator.liu.se', },
    { mac => '84:2b:2b:72:42:91', fqdn => 'trillian-6.lysator.liu.se', },
    { mac => '84:2b:2b:5e:92:1d', fqdn => 'trillian-7.lysator.liu.se', },
    { mac => 'e8:39:35:ef:63:76', fqdn => 'champis.ad.lysator.liu.se', },
    { mac => '9c:b6:54:6d:97:90', fqdn => 'diskus.lysator.liu.se', },
    { mac => '86:24:36:3C:6B:06', fqdn => 'viridian.lysator.liu.se', },
    { mac => '00:14:4f:f2:b9:8c', fqdn => 'diskmaskin.lysator.liu.se', },
  ]

  # Host for which to skip the ipxe boot menu and directly boot a target.
  #
  # This is useful for hosts with buggy firmware that automatically reboots shortly after showing the menu.
  #
  # Hosts defined here must be present in $automatic_hosts above, and refer to a target present in boot.cfg.
  $skip_menu_hosts = [
    { fqdn => 'diskus.lysator.liu.se', target => 'rocky8', },
  ]

  # packages
  package {
    ['isc-dhcp-server', 'nginx', 'tftpd-hpa']:
      ensure => installed,
  }


  # nginx
  file {
    '/srv/www/':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  ['preseed', 'tools'].each  |$dir| {
    file { "/srv/www/${dir}":
      ensure  => directory,
      source  => "puppet:///modules/lyspxeboot/www/${dir}",
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }
  }

  ['ipxe.efi', 'ipxe.pxe'].each |$ipxe_rom| {
    file { "/srv/www/${ipxe_rom}":
      ensure => link,
      target => "/srv/tftp/${ipxe_rom}",
      owner   => 'root',
      group   => 'root',
    }
  }

  file { '/srv/www/boot.cfg':
    ensure  => file,
    content => epp('lyspxeboot/boot.cfg.epp', {
      hosts           => $automatic_hosts,
      skip_menu_hosts => $skip_menu_hosts,
      debian_codename => $debian_codename,
    }),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {
    '/etc/nginx/conf.d/default.conf':
      ensure => absent,
  }

  file {
    '/etc/nginx/conf.d/pxe-server.conf':
      ensure  => file,
      content => template("lyspxeboot/nginx.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  service {
    'nginx':
      ensure    => running,
      subscribe => [File['/etc/nginx/conf.d/pxe-server.conf']],
      enable    => true,
      require   => [Package['nginx'], File['/srv/www/']],
  }

  # tftpd
  file {
    '/etc/default/tftpd-hpa':
      ensure  => file,
      content => template("lyspxeboot/tftpd-hpa.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file {
    '/srv/tftp/':
      ensure  => directory,
      recurse => true,
      source  => "puppet:///modules/lyspxeboot/tftp/",
      owner   => 'root',
      group   => 'nogroup',
      mode    => '0755',
  }

  service {
    'tftpd-hpa':
      ensure    => running,
      subscribe => [File['/etc/default/tftpd-hpa']],
      enable    => true,
      require   => [Package['tftpd-hpa'], File['/srv/tftp/']],
  }

  # dhcpd
  file {
    '/etc/dhcp/dhcpd.conf':
      ensure  => file,
      content => template("lyspxeboot/dhcpd.conf.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file {
    '/etc/default/isc-dhcp-server':
      ensure => file,
      source => "puppet:///modules/lyspxeboot/isc-dhcp-server",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'isc-dhcp-server':
      ensure    => running,
      enable    => true,
      subscribe => [File['/etc/dhcp/dhcpd.conf'], File['/etc/default/isc-dhcp-server']],
  }

  # versions
  $opensuse_version = '15.1'

  # Files required by pxe installers
  file { '/srv/www/installers/':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file {
    '/srv/www/installers/ubuntu1804/':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }
  file {
    "/srv/www/installers/debian-${debian_codename}/":
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }
  file {
    '/srv/www/installers/centos7/':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }
  file {
    "/srv/www/installers/opensuse-${opensuse_version}/":
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  exec { 'ubuntu-1804-initrd':
    command => '/usr/bin/wget -q -O /srv/www/installers/ubuntu1804/initrd.gz http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/ubuntu-installer/amd64/initrd.gz',
    creates => '/srv/www/installers/ubuntu1804/initrd.gz',
    require => [File['/srv/www/installers/ubuntu1804/']],
  }
  exec { 'ubuntu-1804-kernel':
    command => '/usr/bin/wget -q -O /srv/www/installers/ubuntu1804/linux http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/ubuntu-installer/amd64/linux',
    creates => '/srv/www/installers/ubuntu1804/linux',
    require => [File['/srv/www/installers/ubuntu1804/']],
  }

  exec { "debian-${debian_codename}-initrd":
    command => "/usr/bin/wget -q -O /srv/www/installers/debian-${debian_codename}/initrd.gz https://ftp.lysator.liu.se/debian/dists/${debian_codename}/main/installer-amd64/current/images/netboot/debian-installer/amd64/initrd.gz",
    creates => "/srv/www/installers/debian-${debian_codename}/initrd.gz",
    require => [File["/srv/www/installers/debian-${debian_codename}/"]],
  }
  exec { "debian-${debian_codename}-kernel":
    command => "/usr/bin/wget -q -O /srv/www/installers/debian-${debian_codename}/linux https://ftp.lysator.liu.se/debian/dists/${debian_codename}/main/installer-amd64/current/images/netboot/debian-installer/amd64/linux",
    creates => "/srv/www/installers/debian-${debian_codename}/linux",
    require => [File["/srv/www/installers/debian-${debian_codename}/"]],
  }

  exec { 'centos-7-initrd':
    command => '/usr/bin/wget -q -O /srv/www/installers/centos7/initrd.img https://ftp.lysator.liu.se/centos/7/os/x86_64/isolinux/initrd.img',
    creates => '/srv/www/installers/centos7/initrd.img',
    require => [File['/srv/www/installers/centos7/']],
  }
  exec { 'centos-7-kernel':
    command => '/usr/bin/wget -q -O /srv/www/installers/centos7/vmlinuz http://ftp.lysator.liu.se/centos/7/os/x86_64/isolinux/vmlinuz',
    creates => '/srv/www/installers/centos7/vmlinuz',
    require => [File['/srv/www/installers/centos7/']],
  }

  exec { 'opensuse-initrd':
    command => "/usr/bin/wget -q -O /srv/www/installers/opensuse-${opensuse_version}/initrd https://ftp.lysator.liu.se/pub/opensuse/distribution/leap/${opensuse_version}/repo/oss/boot/x86_64/loader/initrd",
    creates => "/srv/www/installers/opensuse-${opensuse_version}/initrd",
    require => [File["/srv/www/installers/opensuse-${opensuse_version}/"]],
  }
  exec { 'opensuse-kernel':
    command => "/usr/bin/wget -q -O /srv/www/installers/opensuse-${opensuse_version}/linux https://ftp.lysator.liu.se/pub/opensuse/distribution/leap/${opensuse_version}/repo/oss/boot/x86_64/loader/linux",
    creates => "/srv/www/installers/opensuse-${opensuse_version}/linux",
    require => [File["/srv/www/installers/opensuse-${opensuse_version}/"]],
  }

  $fedora_version = 40
  file { "/srv/www/installers/fedora${fedora_version}/":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }
  -> exec { 'fedora-kernel':
    command => "/usr/bin/wget -q -O /srv/www/installers/fedora${fedora_version}/vmlinuz https://ftp.lysator.liu.se/pub/fedora/linux/releases/${fedora_version}/Server/x86_64/os/images/pxeboot/vmlinuz",
    creates => "/srv/www/installers/fedora${fedora_version}/vmlinuz",
  }
  -> exec { 'fedora-initrd':
    command => "/usr/bin/wget -q -O /srv/www/installers/fedora${fedora_version}/initrd.img https://ftp.lysator.liu.se/pub/fedora/linux/releases/${fedora_version}/Server/x86_64/os/images/pxeboot/initrd.img",
    creates => "/srv/www/installers/fedora${fedora_version}/initrd.img",
  }

  $rocky_version = 8
  file { "/srv/www/installers/rocky${rocky_version}/":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }
  -> exec { 'rocky-kernel':
    command => "/usr/bin/wget -q -O /srv/www/installers/rocky${rocky_version}/vmlinuz https://ftp.lysator.liu.se/pub/rocky/${rocky_version}/BaseOS/x86_64/os/images/pxeboot/vmlinuz",
    creates => "/srv/www/installers/rocky${rocky_version}/vmlinuz",
  }
  -> exec { 'rocky-initrd':
    command => "/usr/bin/wget -q -O /srv/www/installers/rocky${rocky_version}/initrd.img https://ftp.lysator.liu.se/pub/rocky/${rocky_version}/BaseOS/x86_64/os/images/pxeboot/initrd.img",
    creates => "/srv/www/installers/rocky${rocky_version}/initrd.img",
  }
}
