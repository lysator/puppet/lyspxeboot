#!/bin/sh

# grab our firstboot script
wget -O /root/firstboot http://pxeboot.lysator.liu.se/preseed/ubuntu/preseed.firstboot.sh
chmod +x /root/firstboot

echo "zombie qr" > /root/firstboot.screenrc 

# create a service that will run our firstboot script
cat > /etc/init.d/firstboot <<EOF
#!/bin/sh
### BEGIN INIT INFO
# Provides:        firstboot
# Required-Start:  \$networking
# Required-Stop:   \$networking
# Default-Start:   2 3 4 5
# Default-Stop:    0 1 6
# Short-Description: A script that runs once
# Description: A script that runs once
### END INIT INFO

cd /root ; screen -d -m -S firstboot -c /root/firstboot.screenrc /root/firstboot


EOF

# install the firstboot service
chmod +x /etc/init.d/firstboot
update-rc.d firstboot defaults

# install openssh so the machines become available as soon as possible
apt-get install -y openssh-server
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
systemctl enable ssh.service

echo "finished postinst"
