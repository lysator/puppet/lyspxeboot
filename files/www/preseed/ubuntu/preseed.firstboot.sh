#!/bin/bash -x

exec > >(tee -a /root/install_log)
exec 2>&1

echo "Waiting for network device..."
while ! ifinfo=($(ip -4 -o addr sh permanent primary | awk -F'[ /]+' '$2 != "lo" {print $2" "$4; success=1; exit} END {exit !success}')); do
  sleep 2
done

echo "Found configured network device: ${ifinfo[*]}"

echo "Adding IPv6 address to netplan"
python3 - "${ifinfo[@]}" <<'EOF'
import yaml
import sys

class SaneDumper(yaml.SafeDumper):
    def increase_indent(self, flow=False, indentless=False):
            return super(SaneDumper, self).increase_indent(flow, False)

with open('/etc/netplan/01-netcfg.yaml') as f:
    conf = yaml.safe_load(f)

device, ip4addr = sys.argv[1:]

last_octet = int(ip4addr.split('.')[-1])



conf['network']['ethernets'][device]['addresses'] = list(set(conf['network']['ethernets'][device]['addresses'] + ['2001:6b0:17:f0a0::{:x}/64'.format(last_octet)]))
conf['network']['ethernets'][device]['gateway6'] = '2001:6b0:17:f0a0::1'

conf['network']['ethernets'][device]['nameservers']['addresses'] = list(set(conf['network']['ethernets'][device]['nameservers']['addresses'] + ['2001:6b0:17:f0a0::e1']))

with open('/etc/netplan/01-netcfg.yaml', 'w') as f:
    f.write(yaml.dump(conf, Dumper=SaneDumper, default_flow_style=False, width=float('inf')))
EOF

echo "Applying new netplan"
netplan apply

echo "Waiting for network to become available..."
while ! getent hosts puppet.lysator.liu.se; do
  sleep 2
done

# Remove entry for this host from /etc/hosts
# At this point, there is only an IPv4 entry, which means lookups for the IPv6
# address will fail (and later cause puppet to fail).
# Removing the entry causes lookup to try DNS instead, where both IPv{4,6} are
# available.
sed -i "/$(hostname -f)/d" /etc/hosts

[ -e /tmp/puppet-inst/ ] && rm -rf /tmp/puppet-inst/
git clone --depth 1 git://puppet.lysator.liu.se/ /tmp/puppet-inst
cd /tmp/puppet-inst/

# Some resources will fail the first time, so run puppet twice
# If the install target is used for the first run, it may fail if a scheduled
# run starts before it has finished
make run
make install



update-rc.d -f firstboot remove
rm /root/firstboot
rm /root/firstboot.screenrc
rm /etc/init.d/firstboot
touch /root/install_finished
reboot
