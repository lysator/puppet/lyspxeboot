# Minimum Viable IPv4 resolver
function lyspxeboot::resolve_ipv4 (
  String $hostname,
) >> String {
  inline_template('<%= Resolv::DNS.open { |dns| dns.getaddresses @hostname}.filter { |addr| addr.is_a? Resolv::IPv4 }[0].to_s %>')
}
